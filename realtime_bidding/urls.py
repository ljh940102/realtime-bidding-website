from django.conf.urls import url

from . import views

urlpatterns = [
                url(r'^$', views.main.as_view(),name='index'),
                url(r'^register/$', views.register, name='register'),
                url(r'^register_page/$', views.register_page, name='register_page'),
                url(r'^my_page/$', views.my_page.as_view(), name='my_page'),
                url(r'^bidding_info/(?P<pk>\d+)/$', views.bidding_info.as_view(), name='bidding_info'),
                url(r'^confirm/(?P<pk>\d+)/$', views.confirm, name='confirm'),
                #url(r'^$', views.main)
    ]
