from django import forms

class register_form(forms.Form):

    naver_id = forms.CharField(max_length=20)
    name = forms.CharField(max_length=10)
    password = forms.CharField(widget=forms.PasswordInput)
    password_check = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField(max_length=254)
    phone_num = forms.CharField(max_length=11)
    address = forms.CharField(max_length=254)

class naver_register_form(forms.Form):

    phone_num = forms.CharField(max_length=11)
    address = forms.CharField(max_length=254)