from __future__ import unicode_literals

from django.apps import AppConfig


class RealtimeBiddingConfig(AppConfig):
    name = 'realtime_bidding'
