from __future__ import unicode_literals
from django.utils import timezone

from django.db import models
from django.contrib.auth.models import User as auth_User
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser)
# Create your models here.

class User(models.Model):
    naver_id=models.CharField(max_length=20,primary_key=True)
    email=models.EmailField()
    #phone_num = models.PositiveIntegerField()

    def __unicode__(self):
        return self.naver_id

class MyUserManager(BaseUserManager):
    def create_user(self, naver_id, phone_num, address,name, email, password=None):

        if not naver_id:
            raise ValueError('Users must have an naver account')

        user = self.model(
            naver_id=naver_id,
            address=address,
            phone_num=phone_num,
            name=name,
            email=email,
        )

        user.is_admin = False
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, naver_id, phone_num, address, name, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.model(
            naver_id=naver_id,
            address=address,
            phone_num=phone_num,
            password=password,
            name=name,
            email=email
        )

        user.is_admin = True
        user.save(using=self._db)

        return user

class test_user(AbstractBaseUser):
    naver_id= models.CharField(max_length=20,null=True,blank=True)
    name=models.CharField(max_length=10,default="")
    email = models.EmailField(max_length=254,default="",unique=True)
    phone_num = models.CharField(max_length=11)
    address = models.CharField(max_length=254)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_num', 'address','name']

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def get_full_name(self):
        # The user is identified by their email address
        return self.name

    def get_short_name(self):
        # The user is identified by their email address
        return self.name

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Product(models.Model):
    product_id=models.AutoField(primary_key=True)
    chatting_room_id=models.ManyToManyField('Chatting_room')
    product_name=models.CharField(max_length=50)
    start_price=models.PositiveIntegerField(default=0)
    bidding_price=models.PositiveIntegerField(null=True,blank=True)
    seller_id=models.ManyToManyField(test_user,related_name='seller_id')
    buyer_id=models.EmailField(max_length=254,default="")
    information=models.CharField(max_length=200,blank=True)
    pro_image = models.ImageField(upload_to = "realtime_bidding/static/img/",null=True, blank=True)
    bidding_state=models.PositiveIntegerField(default=1) # 1 is ing, 2 is waiting for deposit 3 is done. later maybe add more state

    def __unicode__(self):
        return self.product_name

class Chatting_room(models.Model):
    room_id=models.AutoField(primary_key=True)
    chatting_history_file_path=models.CharField(max_length=300)
    bidding_history_file_path=models.CharField(max_length=300)
    room_created=models.DateTimeField(auto_now_add=True)
    rest_of_bidding = models.DateTimeField(default=timezone.localtime(timezone.now()))
    chat_done = models.BooleanField(default=False)
    bid_start = models.BooleanField(default=False)
    #chatting_history_file_path=models.FilePathField(path=settings.FILE_PATH_FIELD_DIRECTORY)
    #bidding_history_file_path=models.FilePathField(path=settings.FILE_PATH_FIELD_DIRECTORY)

    def __unicode__(self):
        return '%s' % (self.room_id)

class Chat(models.Model):
    chat_room = models.ForeignKey(Chatting_room, on_delete=models.CASCADE, default=9999)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(test_user)
    message = models.CharField(max_length=200)

    def __unicode__(self):
        return self.message

class bidding_Chat(models.Model):
    chat_room = models.ForeignKey(Chatting_room, on_delete=models.CASCADE, default=9999)
    created = models.DateTimeField(blank=True)
    user = models.ForeignKey(test_user)
    message = models.CharField(max_length=200)

    def __unicode__(self):
        return self.message