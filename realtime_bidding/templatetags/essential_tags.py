from django import template

register = template.Library()

@register.assignment_tag
def set_plusone(num):
    num1 = num + 1
    return num1

@register.assignment_tag
def set_plusthree(num):
    num1 = num + 3
    return num1