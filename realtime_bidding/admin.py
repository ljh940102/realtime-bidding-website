from django.contrib import admin
from realtime_bidding.models import User,Product,Chatting_room,bidding_Chat,Chat, test_user

# Register your models here.

admin.site.register(User)
admin.site.register(Product)
admin.site.register(Chatting_room)
admin.site.register(bidding_Chat)
admin.site.register(test_user)
