from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render
from django.template import loader, Context
from django.views.generic import ListView, DetailView
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, logout, login

from realtime_bidding.forms import register_form
from .models import Chatting_room , Product , test_user , MyUserManager


# Create your views here.

class main(ListView):

    template_name = 'index.html'
    page_num = 0
    queryset = Chatting_room.objects.all().filter(chat_done = False).order_by('room_id')
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super(main, self).get_context_data(**kwargs)

        if self.request.user.is_authenticated() :
            user_email = self.request.user.email
            try:
                context['test_user'] = test_user.objects.get(email=user_email)
            except test_user.DoesNotExist:
                context['is_naver_account'] = "not"
        else:
            pass
        return context


def register_page(request):
    return render(request,'register_page.html')

def register(request):
    if request.method == 'POST':
        form = register_form(request.POST)
        if form.is_valid():

            naver_id = form.cleaned_data.get('naver_id')
            phone_num = form.cleaned_data.get('phone_num')
            name = form.cleaned_data.get('name')
            address = form.cleaned_data.get('address')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            password_check = form.cleaned_data.get('password_check')

            try:
                id_check = test_user.objects.get(email=email)
            except test_user.DoesNotExist:
                if password != password_check:
                    return HttpResponse("wrong password check! please more check")

                user = test_user.objects.create_user(naver_id, phone_num, address, name, email, password)
                user.save()
                return render(request, 'register_success.html')

            return HttpResponse("you already register in our site! please check!")

    else:
        form = register_form()
        token = request.GET.get('token','token sending error! please try again')
    return render(request, 'register.html', {'form': form })

class my_page(TemplateView):

    template_name = 'my_page.html'

    def get_context_data(self, **kwargs):
        context = super(my_page, self).get_context_data(**kwargs)

        if not self.request.user.is_authenticated():
            return HttpResponse('You must log in !')

        login_user = test_user.objects.get(email=self.request.user.email)
        context['sell_product'] = Product.objects.all().filter(seller_id=login_user).order_by('bidding_state')

        try:
            context['buy_product'] = Product.objects.all().filter(buyer_id=login_user).order_by('bidding_state')
        except :
            pass

        return context

class bidding_info(DetailView) :

    template_name = "bidding_info.html"

    model = Product

    def get_context_data(self, **kwargs):

        context = super(bidding_info, self).get_context_data(**kwargs)

        context['seller'] = Product.objects.get(product_id=self.kwargs['pk']).seller_id.all()
        context['buyer'] = Product.objects.get(product_id=self.kwargs['pk']).buyer_id
        context['test'] = Product.objects.get(product_id=self.kwargs['pk']).seller_id

        return context

def confirm(request, pk):
    product = Product.objects.get(product_id=pk)
    product.bidding_state="3"
    product.save()

    #print(product.chatting_room_id.all()[0])

    return HttpResponseRedirect('../../my_page')