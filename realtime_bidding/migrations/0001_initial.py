# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-12-13 09:33
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='test_user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('naver_id', models.CharField(blank=True, max_length=20, null=True)),
                ('name', models.CharField(default='', max_length=10)),
                ('email', models.EmailField(default='', max_length=254, unique=True)),
                ('phone_num', models.CharField(max_length=11)),
                ('address', models.CharField(max_length=254)),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='bidding_Chat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(blank=True)),
                ('message', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('message', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Chatting_room',
            fields=[
                ('room_id', models.AutoField(primary_key=True, serialize=False)),
                ('chatting_history_file_path', models.CharField(max_length=300)),
                ('bidding_history_file_path', models.CharField(max_length=300)),
                ('room_created', models.DateTimeField(auto_now_add=True)),
                ('rest_of_bidding', models.DateTimeField(default=datetime.datetime(2016, 12, 13, 9, 33, 16, 487000, tzinfo=utc))),
                ('chat_done', models.BooleanField(default=False)),
                ('bid_start', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('product_id', models.AutoField(primary_key=True, serialize=False)),
                ('product_name', models.CharField(max_length=50)),
                ('start_price', models.PositiveIntegerField(default=0)),
                ('bidding_price', models.PositiveIntegerField(blank=True, null=True)),
                ('buyer_id', models.EmailField(default='', max_length=254)),
                ('information', models.CharField(blank=True, max_length=200)),
                ('pro_image', models.ImageField(blank=True, null=True, upload_to='realtime_bidding/static/img/')),
                ('bidding_state', models.PositiveIntegerField(default=1)),
                ('chatting_room_id', models.ManyToManyField(to='realtime_bidding.Chatting_room')),
                ('seller_id', models.ManyToManyField(related_name='seller_id', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('naver_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.AddField(
            model_name='chat',
            name='chat_room',
            field=models.ForeignKey(default=9999, on_delete=django.db.models.deletion.CASCADE, to='realtime_bidding.Chatting_room'),
        ),
        migrations.AddField(
            model_name='chat',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bidding_chat',
            name='chat_room',
            field=models.ForeignKey(default=9999, on_delete=django.db.models.deletion.CASCADE, to='realtime_bidding.Chatting_room'),
        ),
        migrations.AddField(
            model_name='bidding_chat',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
