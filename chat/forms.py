from django import forms

class testform(forms.Form):

    pro_name = forms.CharField(label='pro_name',max_length=50)
    pro_price = forms.IntegerField(label='pro_price',min_value=0)
    pro_info = forms.CharField(label='pro_info',max_length=200)
    pro_image = forms.ImageField(label='pro_image')