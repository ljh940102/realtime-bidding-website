from __future__ import print_function
from django.shortcuts import render
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from bidding import settings
from django.views.generic import DetailView,ListView
from django.views.generic.base import TemplateView
from datetime import timedelta, date
from django.utils.dateformat import DateFormat
from django.utils.formats import get_format
from django.utils import timezone
from chat.forms import testform
from PIL import Image
from django.utils import timezone
import datetime

from realtime_bidding.models import Chat, Chatting_room, test_user, Product

from django.conf import settings;

def Login(request, room_number=None):
    print("-------------------")
    print(timezone.now())
    print(datetime.datetime.now())

    if room_number is None:
        next = request.GET.get('next', '../../')
    else:
        next = request.GET.get('next', '/chat/home/' + str(room_number))

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next)
            else:
                return HttpResponse("Account is not active at the moment.")
        else:
            return HttpResponse("please register!")
    return render(request, "chat/login.html", {'next': next})


def Logout(request):
    logout(request)
    return HttpResponseRedirect('../../')

class Home(DetailView):
    template_name = 'chat/home.html'
    model = Chatting_room

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)

        td = timedelta(minutes=10)
        td_1 = timedelta(hours=9)
        desired_format = '%Y-%b-%d-%H-%M-%S'

        start_of_bidding = Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).room_created #stating time of bidding
        compare_time = start_of_bidding + td


        if(compare_time>=timezone.localtime(timezone.now())) :
            context['is_10min'] = "not show"
        else :
            context['is_10min'] = "show"

        context['start_of_bidding'] = start_of_bidding
        context['chat'] = Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).chat_set.all()
        context['bidding_chat'] = Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).bidding_chat_set.all()
        context['product'] = Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).product_set.all()
        context['user_me'] = self.request.user
        context['home'] = 'active'
        context['room_number'] = self.kwargs['pk']
        return context

    def render_to_response(self, context):

        if not self.request.user.is_authenticated():
            return HttpResponse('you must register for using our service!')

        return super(Home, self).render_to_response(context)


def Post(request,room_number):
    if request.method == "POST":
        msg = request.POST.get('msgbox', None)
        chat_room = Chatting_room.objects.get(room_id=int(room_number))

        if msg != '':
            c = chat_room.chat_set.create(user=request.user, message=msg)

        return JsonResponse({ 'msg': msg, 'user': c.user.name })
    else:
        return HttpResponse('Request must be POST.')


def Bidding_Post(request,room_number):
    if request.method == "POST":
        msg = request.POST.get('msgbox', None)
        chat_room = Chatting_room.objects.get(room_id=int(room_number))
        product = Chatting_room.objects.get(room_id=int(room_number)).product_set.all()
        login_user = test_user.objects.get(email=request.user.email)
        start_of_bidding = chat_room.room_created  # stating time of bidding

        td = timedelta(minutes=10)
        td_1 = timedelta(minutes=1)
        desired_format = '%H-%M-%S'
        compare_time = start_of_bidding + td

        if (compare_time >= timezone.localtime(timezone.now())):
            is_10min_post = "not show"
        else:
            is_10min_post = "show"

        if(chat_room.rest_of_bidding < timezone.localtime(timezone.now()) and is_10min_post=="show") :

            chat_room.chat_done=True
            chat_room.save()
            product.update(bidding_state="2")
            return JsonResponse({'chat_is_done' : "chat_is_done" })

        if msg != '' :
            msg = int(msg)
            if msg <= product.latest('product_id').bidding_price:
                bigger_msg = "You must suggest bigger price than now bidding price"
                return JsonResponse({'bigger_msg': bigger_msg})
            else:
                product.update(bidding_price=msg)
                product.update(buyer_id=login_user.email)
                c = chat_room.bidding_chat_set.create(user=request.user, message=msg,created=timezone.localtime(timezone.now()))
                rest_of_biddingtime_temp = c.created + td_1
                chat_room.rest_of_bidding = rest_of_biddingtime_temp
                chat_room.save()

        return JsonResponse({ 'msg': msg, 'user': c.user.name, 'bid_time' : rest_of_biddingtime_temp.strftime(desired_format),'is_10min_post' : is_10min_post,})
    else:
        return HttpResponse('Request must be POST.')


class Messages(DetailView):
    template_name = 'chat/messages.html'

    model = Chatting_room

    def get_context_data(self, **kwargs):
        context = super(Messages, self).get_context_data(**kwargs)
        context['chat'] = Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).chat_set.all()
        return context

class Bidding_Messages(DetailView):
    template_name = 'chat/bidding_messages.html'

    model = Chatting_room

    def get_context_data(self, **kwargs):
        context = super(Bidding_Messages, self).get_context_data(**kwargs)
        context['bidding_chat']= Chatting_room.objects.get(room_id=int(self.kwargs['pk'])).bidding_chat_set.all()
        return context

def new_chatroom_product(request):

    if not request.user.is_authenticated():
        return HttpResponse('you must register for using our service!')

    if request.method == 'POST':
        form = testform(request.POST, request.FILES)

        if form.is_valid():
            try:
                latest_room = Chatting_room.objects.latest('room_id')
                latest_room_id = latest_room.room_id
            except:
                latest_room_id = -1

            new_room_id = latest_room_id + 1

            Chatting_room.objects.create(room_id=new_room_id)
            chat_room = Chatting_room.objects.get(room_id=new_room_id)
            seller_user = test_user.objects.get(email=request.user.email)

            chat_room.product_set.create(
                product_name=form.cleaned_data.get('pro_name'),
                start_price=form.cleaned_data.get('pro_price'),
                bidding_price=form.cleaned_data.get('pro_price'),
                information=form.cleaned_data.get('pro_info'),
                pro_image=request.FILES['pro_image'],
            )

            new_product = chat_room.product_set.get(chatting_room_id=new_room_id)
            new_product.seller_id.add(seller_user)
            return render(request, 'chat/create_new_room.html', {'room_number': new_room_id})
    else:
        form = testform()
    return render(request, 'chat/name.html', {'form': form})
